'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlunoSchema = new Schema({
    nome: {
        type: String,
        required: 'Kindly enter the name'
    },
    turma_id: {
        type: String,
        required: true
    },
    sobrenome: {
        type: String,
        required: 'Kindly enter the name'
    },
    data_criacao: {
        type: Date,
        default: Date.now
    },
    data_nascimento: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    },
    presente: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Aluno', AlunoSchema);