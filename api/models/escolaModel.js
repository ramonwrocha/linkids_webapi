'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EscolaSchema = new Schema({
    descricao: {
        type: String,
        required: 'Kindly enter the name'
    },
    data_criacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Escola', EscolaSchema);