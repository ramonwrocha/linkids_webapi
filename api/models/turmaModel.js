'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TurmaSchema = new Schema({
    descricao: {
        type: String,
        required: 'Kindly enter the name'
    },
    escola_id: {
        type: String
    },
    data_criacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Turma', TurmaSchema);