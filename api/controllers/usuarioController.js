"use strict";

var mongoose = require("mongoose"),
    jwt = require("jsonwebtoken"),
    bcrypt = require("bcrypt"),
    Usuario = mongoose.model("Usuario");

var _url = "*";
var _cors = "Access-Control-Allow-Origin";

//crud default

exports.inserir = function(req, res) {
    res.setHeader(_cors, _url);

    if (req.body.email && req.body.username && req.body.password && req.body.passwordConf) {

        var usuarioData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            passwordConf: req.body.passwordConf,
        }

        Usuario.create(usuarioData, function(err, user) {

            if (err)
                return next(err)

            return res.redirect('/portaria');

        });
    }

};

exports.registrar = function(req, res) {

    var newUser = new Usuario(req.body);

    newUser.hash_password = bcrypt.hashSync(req.body.password, 10);

    newUser.save(function(err, user) {

        if (err) {
            return res.status(400).send({
                message: err
            });
        } else {
            user.hash_password = undefined;
            return res.json(user);
        }

    });
}

exports.autenticar = function(req, res) {

    res.setHeader(_cors, _url);

    var obj = JSON.parse(Object.keys(req.body)[0]);

    Usuario.findOne({ username: obj.username }, function(err, user) {

        if (err) throw err;

        if (!user) {

            res.status(401).json({ message: 'Authentication failed. Usuario not found.' });

        } else if (user) {

            if (!user.comparePassword(obj.password)) {

                res.status(401).json({ message: 'Authentication failed. Wrong password.' });

            } else {

                return res.json({ token: jwt.sign({ email: user.email, username: user.username, _id: user._id }, 'RESTFULAPIs'), email: user.email, username: user.username, _id: user._id });

            }

        }

    });
}

exports.loginRequired = function(req, res, next) {

    if (req.user) {
        next();
    } else {
        return res.status(401).json({ message: 'Unauthorized user!' });
    }

}