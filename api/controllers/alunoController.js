"use strict";

var mongoose = require("mongoose"),
    Aluno = mongoose.model("Aluno");

var _url = "*";
var _cors = "Access-Control-Allow-Origin";

//crud default
exports.obter_todos = function(req, res) {
    res.setHeader(_cors, _url);

    Aluno.find({}, function(err, alunos) {
        if (err) res.send(err);
        res.json(alunos);
    });

};

exports.inserir = function(req, res) {
    res.setHeader(_cors, _url);

    var novoAluno = new Aluno(req.body);

    novoAluno.save(function(err, aluno) {
        if (err) res.send(err);
        res.json(aluno);
    });
};

exports.obter = function(req, res) {
    res.setHeader(_cors, _url);

    Aluno.findById(req.params.alunoId, function(err, aluno) {
        if (err) res.send(err);
        res.json(aluno);
    });
};

exports.atualizar = function(req, res, model) {
    res.setHeader(_cors, _url);

    // Aluno.findOneAndUpdate({ _id: req.body._id },
    //     req.body, { new: true },
    //     function(err, aluno) {
    //         if (err) res.send(err);
    //         res.json(aluno);
    //     }
    // );

    var obj = JSON.parse(Object.keys(req.body)[0]);

    Aluno.findOneAndUpdate({ _id: obj._id },
        obj, { new: true },
        function(err, aluno) {
            if (err) res.send(err);
            res.json(aluno);
        }
    );
};

exports.deletar = function(req, res) {
    res.setHeader(_cors, _url);

    Aluno.remove({
            _id: req.params.alunoId
        },
        function(err, aluno) {
            if (err) res.send(err);
            res.json({ message: "Aluno deletado" });
        }
    );
};

//app
exports.obterPorTurma = function(req, res) {
    res.setHeader(_cors, _url);

    Aluno.find({ turmaId: req.params.turmaId }, function(err, aluno) {
        if (err) res.send(err);
        res.json(aluno);
    });

};

exports.obterPresentes = function(req, res) {
    res.setHeader(_cors, _url);

    Aluno.find({ presente: true }, function(err, result) {
        if (err) res.json(err);
        res.json(result);
    });

};

exports.obterAusentes = function(req, res) {
    res.setHeader(_cors, _url);

    Aluno.find({ presente: false }, function(err, result) {
        if (err) res.json(err);
        res.json(result);
    });

};