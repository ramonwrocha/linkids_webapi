'use strict';

var mongoose = require('mongoose'),
    Turma = mongoose.model('Turma');

var _url = "*";
var _cors = "Access-Control-Allow-Origin";

exports.obter_todos = function(req, res) {
    res.setHeader(_cors, _url);

    Turma.find({}, function(err, turma) {
        if (err)
            res.send(err);
        res.json(turma);
    });

};

exports.inserir = function(req, res) {

    var new_turma = new Turma(req.body);
    new_turma.save(function(err, turma) {
        if (err)
            res.send(err);
        res.json(turma);
    });

};

exports.obter = function(req, res) {

    Turma.findById({ _id: req.params.turmaId }, function(err, turma) {
        if (err)
            res.send(err);
        res.json(turma);
    });

};

exports.atualizar = function(req, res) {

    Turma.findOneAndUpdate({ _id: req.params.turmaId }, req.body, { new: true }, function(err, turma) {
        if (err)
            res.send(err);
        res.json(turma);
    });

};

exports.deletar = function(req, res) {

    Turma.remove({
        _id: req.params.turmaId
    }, function(err, turma) {
        if (err)
            res.send(err);
        res.json({ message: 'Turma successfully deleted' });
    });

};