'use strict';

var mongoose = require('mongoose'),
    Escola = mongoose.model('Escola');

exports.obter_todos = function(req, res) {

    Escola.find({}, function(err, escola) {
        if (err)
            res.send(err);
        res.json(escola);
    });

};

exports.inserir = function(req, res) {

    var new_turma = new Escola(req.body);
    new_turma.save(function(err, escola) {
        if (err)
            res.send(err);
        res.json(escola);
    });

};

exports.obter = function(req, res) {

    Escola.findById({ _id: req.params.turmaId }, function(err, escola) {
        if (err)
            res.send(err);
        res.json(escola);
    });

};

exports.atualizar = function(req, res) {

    Escola.findOneAndUpdate({ _id: req.params.turmaId }, req.body, { new: true }, function(err, escola) {
        if (err)
            res.send(err);
        res.json(escola);
    });

};

exports.deletar = function(req, res) {

    Escola.remove({
        _id: req.params.turmaId
    }, function(err, escola) {
        if (err)
            res.send(err);
        res.json({ message: 'Escola successfully deleted' });
    });

};