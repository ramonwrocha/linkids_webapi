'use strict';
module.exports = function(app) {
    var usuario = require('../controllers/usuarioController');

    // usuario Routes
    app.route('/usuario')
        .post(usuario.inserir);

    app.route('/usuario/registrar')
        .post(usuario.registrar);

    app.route('/usuario/autenticar')
        .post(usuario.autenticar);

};