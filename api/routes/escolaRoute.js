'use strict';
module.exports = function(app) {
    var escola = require('../controllers/escolaController');

    // escola Routes
    app.route('/escola')
        .get(escola.obter_todos)
        .post(escola.inserir);

    app.route('/escola/:escolaId')
        .get(escola.obter)
        .delete(escola.deletar);

    app.route('/atualizar')
        .post(escola.atualizar);

};