'use strict';
module.exports = function(app) {
    var aluno = require('../controllers/alunoController');

    // aluno Routes
    app.route('/alunos')
        .get(aluno.obter_todos)
        .post(aluno.inserir);

    app.route('/obterAlunosAusentes')
        .get(aluno.obterAusentes);

    app.route('/obterAlunosPresentes')
        .get(aluno.obterPresentes);

    app.route('/aluno/:alunoId')
        .get(aluno.obter)
        .delete(aluno.deletar);

    app.route('/alunos/obterPorTurma/:turmaId')
        .get(aluno.obterPorTurma);

    app.route('/atualizar')
        .post(aluno.atualizar);

};
//59fe11a069c17c28783361b2 turma_id
//59c5a1c01e47b231045d9332 aluno_id
/*
app.route('/aluno/atualizar')
        .post(aluno.atualizar);
        */