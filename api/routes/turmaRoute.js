'use strict';
module.exports = function(app) {
    var turma = require('../controllers/turmaController');

    // turma Routes
    app.route('/turmas')
        .get(turma.obter_todos)
        .post(turma.inserir);

    app.route('/turma/:turmaId')
        .get(turma.obter)
        .delete(turma.deletar);

    app.route('/atualizar')
        .post(turma.atualizar);

};