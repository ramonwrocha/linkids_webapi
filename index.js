var express = require("express"),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require("mongoose"),
    Aluno = require("./api/models/alunoModel"),
    Usuario = require("./api/models/usuarioModel"),
    Turma = require('./api/models/turmaModel'),
    Escola = require('./api/models/escolaModel'),
    bodyParser = require("body-parser"),
    jsonwebtoken = require("jsonwebtoken");

var dev = false;

var urlBanco = dev ? 'mongodb://localhost/linkids_db' : 'mongodb://admin:admin@ds121456.mlab.com:21456/linkids_db';

mongoose.Promise = global.Promise;
mongoose.connect(urlBanco, {
    useMongoClient: true
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {

    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {

        jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function(err, decode) {
            if (err) req.user = undefined;
            req.user = decode;
            next();
        });

    } else {

        req.user = undefined;
        next();

    }

});

var alunoRoute = require('./api/routes/alunoRoute');
var turmaRoute = require('./api/routes/turmaRoute');
var escolaRoute = require('./api/routes/escolaRoute');
var usuarioRoute = require('./api/routes/usuarioRoute');

alunoRoute(app);
turmaRoute(app);
escolaRoute(app);
usuarioRoute(app);

app.use(function(req, res) {
    res.status(404).send({ url: req.originalUrl + ' not found (NÃO ENCONTRADO)' });
});

app.listen(port);

console.log("Linkids RESTful API server started on: " + port);